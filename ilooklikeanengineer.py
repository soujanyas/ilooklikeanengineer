NUM_LINES=8
CAPS_DIFF=2
SMALL_LETTER_CENTER=CAPS_DIFF+(NUM_LINES-CAPS_DIFF)/2

buflist = [""] * NUM_LINES

def horizontal_line( index, len ):
    for i in xrange( len ):
        pipe( index )

def pipe( index ):
    buflist[ index ] += "|"

def space( index ):
    buflist[ index ] += " "

def I():
    for line in xrange( 0, NUM_LINES ):
        horizontal_line( line, 4 )

def i():
    small_letter( 3 )
    for line in xrange( CAPS_DIFF, NUM_LINES ):
        horizontal_line( line, 3 )


def L():
    box_len = 9
    for line in xrange( 0, NUM_LINES-1 ):
        horizontal_line( line, 3 )
        empty_line( line, box_len-3 )
    horizontal_line( NUM_LINES-1, box_len )

def centered_line( index, num_space, pipe_len ):
    for i in xrange( num_space ):
        space( index )
    horizontal_line( index, pipe_len )
    for i in xrange( num_space ):
        space( index )

def empty_line( index, len ):
    for i in xrange( len ):
        buflist[ index ] += " "

def small_letter( box_len ):
    for line in xrange( 0, CAPS_DIFF ):
        empty_line( line, box_len )

def horizontal_slash( index, len ):
    for i in xrange( len ):
        buflist[ index ] += "/"

def horizontal_back_slash( index, len ):
    for i in xrange( len ):
        buflist[ index ] += "\\"

def o():

    small_letter( 3+2+3 )
    centered_line( CAPS_DIFF, 2, 4 )
    for line in xrange( CAPS_DIFF+1, NUM_LINES-1 ):
        horizontal_line( line, 3 )
        space( line )
        space( line )
        horizontal_line( line, 3 )

    centered_line( NUM_LINES-1, 2, 4 )

def k():
    box_len = 3+2+2
    small_letter( box_len )
    i=2
    for line in xrange( CAPS_DIFF, SMALL_LETTER_CENTER ):
        horizontal_line( line, 3 )
        empty_line( line, i )
        i -= 1
        horizontal_slash( line, 2 )
        empty_line( line, box_len - (3+i+2)-1 ) # fill in spaces for completing the box.
    i=0
    for line in xrange( SMALL_LETTER_CENTER, NUM_LINES ):
        horizontal_line( line, 3 )
        empty_line( line, i )
        horizontal_back_slash( line, 2 )
        empty_line( line, box_len - (3+i+2) ) # fill in spaces for completing the box.
        i += 1

def horizontal_flat_line( index, length ):
    for j in xrange( length ):
        buflist[ index ] += "_"


def e():
    small_letter( 4 )
    i() #E's left
    horizontal_slash( CAPS_DIFF, 4 )
    for line in xrange( CAPS_DIFF+1, SMALL_LETTER_CENTER-1 ):
        empty_line( line, 4 )
    # E's center line
    horizontal_flat_line( SMALL_LETTER_CENTER-1, 4)
    # E's bottom
    for line in xrange( SMALL_LETTER_CENTER, NUM_LINES-1 ):
        empty_line( line, 4 )

    horizontal_slash( NUM_LINES-1, 4 )

def E():
    I()
    horizontal_slash( 0, 5 )
    for line in xrange(1, NUM_LINES/2-1 ):
        empty_line( line, 5 )
    horizontal_slash( (NUM_LINES/2)-1, 5 )
    horizontal_slash( NUM_LINES/2, 5 )
    for line in xrange( NUM_LINES/2+1, NUM_LINES-1 ):
        empty_line( line, 5 )
    horizontal_slash( NUM_LINES-1, 5 )

def g():
    small_letter( 10 )
    centered_line( CAPS_DIFF, 2, 5 )
    space( CAPS_DIFF )
    for line in xrange( CAPS_DIFF+1, NUM_LINES-1 ):
        horizontal_line( line, 3 )
        if line == CAPS_DIFF+1:
            empty_line( line, 7 )
        elif line == CAPS_DIFF+2:
            empty_line( line, 2 )
            horizontal_line( line, 5 )
        else:
            empty_line( line, 3 )
            horizontal_line( line, 3 )
            empty_line( line, 1 )
    centered_line( NUM_LINES-1, 2, 5 )
    space( NUM_LINES-1 )

def r():
    small_letter( 7 )
    i()
    horizontal_slash( CAPS_DIFF, 5 )
    pipe( CAPS_DIFF )
    space( CAPS_DIFF )

    for line in xrange( CAPS_DIFF+1, SMALL_LETTER_CENTER ):
        if line == SMALL_LETTER_CENTER-1:
            horizontal_flat_line( line, 4 )
        else:
            empty_line( line, 4 )
        horizontal_line( line, 3 )


    box_len = 10
    j=1
    for line in xrange( SMALL_LETTER_CENTER, NUM_LINES ):
        empty_line( line, j )
        horizontal_back_slash( line, 3 )
        empty_line( line, box_len - (3+j+3) ) # fill in spaces for completing the box.
        j += 1



def A():
    centered_line( 0, 2, 7 )
    for line in xrange( 1, NUM_LINES ):
        horizontal_line( line, 4 )
        if line== SMALL_LETTER_CENTER-1:
            horizontal_flat_line( line, 3 )
        else:
            empty_line( line, 3 )
        horizontal_line( line, 4 )

def back_slash( box_len ):
    horizontal_back_slash( CAPS_DIFF, 1 ) #todo: Make generic
    empty_line( CAPS_DIFF, box_len-1 )

    # In between, follows a pattern
    i=0
    for line in xrange( CAPS_DIFF+1, NUM_LINES-1 ):
        empty_line( line, i )
        horizontal_back_slash( line, 3 )
        empty_line( line, box_len - 3 - i )
        i += 1

    empty_line( NUM_LINES-1, box_len-2 )
    horizontal_back_slash( NUM_LINES-1, 2 )

def n():
    small_letter( 6)
    i()
    back_slash( 6 )
    i()

def word_space():
    for line in xrange( NUM_LINES ):
        buflist[ line ] += " "

def print_buflist():
    for line in xrange( NUM_LINES ):
        print buflist[ line ]


if __name__ == "__main__":
    
    for l in xrange( 3 ):
        print("")
    
    for l in xrange( 3 ):
        word_space()
    I()
    word_space()
    word_space()
    L()
    word_space()
    o()
    word_space()
    o()
    word_space()
    k()
    word_space()
    L()
    word_space()
    i()
    word_space()
    k()
    word_space()
    e()
    word_space()
    A()
    word_space()
    n()
    word_space()

    print_buflist()
    buflist = [""] * NUM_LINES

    for l in xrange( 3 ):
        word_space()

    E()
    word_space()
    n()
    word_space()
    g()
    word_space()
    i()
    word_space()
    n()
    word_space()
    e()
    word_space()
    e()
    word_space()
    r()
    word_space()

    print("")
    print_buflist()

    for l in xrange( 3 ):
        print("")


    #l()
    #o()



